trait Worker {
    type MessageIn;
    type MessageOut;

    fn init(self, post_message: impl Fn(Self::MessageOut), close: impl Fn());

    fn on_message(
        &mut self,
        message: Self::MessageIn,
        post_message: impl Fn(Self::MessageOut),
        close: impl Fn()
    );
}

trait SimpleWorker {
    type MessageOut;

    fn run(self) -> Self::MessageOut;
}

impl<S: SimpleWorker> Worker for S {
    type MessageIn = ();
    type MessageOut = S::MessageOut;

    fn init(self, post_message: impl Fn(Self::MessageOut), close: impl Fn()) {
        let message_out = S::run(self);
        post_message(message_out);
        close();
    }

    fn on_message(
        &mut self,
        message: Self::MessageIn,
        post_message: impl Fn(Self::MessageOut),
        close: impl Fn()
    ) {}
}

fn new_worker(worker: impl Worker)