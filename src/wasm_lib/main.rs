#![recursion_limit = "128"]

use stdweb::{__js_raw_asm, js_export};

use mines_rs::{GameBatch, NativeServer, SpecResult};

fn main() {
    sample();
}

#[js_export]
fn run_batch(batch: GameBatch<Vec<usize>, Vec<usize>>) -> Vec<SpecResult<()>> {
    batch
        .run(|spec| NativeServer::new(spec, false), |_game| ())
        .unwrap()
}

//DELETEME
fn sample() {
    use stdweb::{_js_impl, js};

    js! {
        Rust.mines_rs_wasm_lib
            .then(lib => {
                const batch = {
                    count_per_spec: 100,
                    dims_range: [[20], [20]],
                    mines_range: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
                    autoclear: true,
                    metaseed: 13337
                };

                console.log("Running sample...");

                return lib.run_batch(batch);
            })
            .then(results => {
                console.table(results.map(({ mines, played, wins }) => ({ mines, played, wins })));
            });
    }
}
