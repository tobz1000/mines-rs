WebAssembly.Module: compiled wasm code unit. Can be compiled with WebAssembly.compileStreaming.

```js
// main.js

const worker = new Worker("workerLoad.js");
const mod = await WebAssembly.compileStreaming(fetch("worker.wasm"));
worker.postMessage(mod);
```

```js
// workerLoad.js

onmessage = mod => {
    const instance = await WebAssembly.instantiate(mod);
    instance.exports.some_exported_wasm_function();
}
```